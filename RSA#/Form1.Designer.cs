﻿namespace RSA
{
	partial class Form1
	{
		/// <summary>
		/// Обязательная переменная конструктора.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		/// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Код, автоматически созданный конструктором форм Windows

		/// <summary>
		/// Требуемый метод для поддержки конструктора — не изменяйте 
		/// содержимое этого метода с помощью редактора кода.
		/// </summary>
		private void InitializeComponent()
		{
            this.TextBox1 = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.Button2 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.Button1 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.Button3 = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.OpenTextField1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.TextBox3 = new System.Windows.Forms.RichTextBox();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.CloseTextField2 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.SuspendLayout();
            // 
            // TextBox1
            // 
            this.TextBox1.Location = new System.Drawing.Point(18, 122);
            this.TextBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBox1.Name = "TextBox1";
            this.TextBox1.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.TextBox1.Size = new System.Drawing.Size(758, 110);
            this.TextBox1.TabIndex = 0;
            this.TextBox1.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(18, 486);
            this.richTextBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richTextBox2.Size = new System.Drawing.Size(758, 120);
            this.richTextBox2.TabIndex = 1;
            this.richTextBox2.Text = "";
            // 
            // Button2
            // 
            this.Button2.Depth = 0;
            this.Button2.Location = new System.Drawing.Point(784, 486);
            this.Button2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button2.MouseState = MaterialSkin.MouseState.HOVER;
            this.Button2.Name = "Button2";
            this.Button2.Primary = true;
            this.Button2.Size = new System.Drawing.Size(141, 120);
            this.Button2.TabIndex = 5;
            this.Button2.Text = "Расшифровать";
            this.Button2.UseVisualStyleBackColor = true;
            this.Button2.Click += new System.EventHandler(this.MaterialRaisedButton2_Click);
            // 
            // Button1
            // 
            this.Button1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.Button1.Depth = 0;
            this.Button1.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.Button1.Location = new System.Drawing.Point(784, 293);
            this.Button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button1.MouseState = MaterialSkin.MouseState.HOVER;
            this.Button1.Name = "Button1";
            this.Button1.Primary = true;
            this.Button1.Size = new System.Drawing.Size(141, 125);
            this.Button1.TabIndex = 6;
            this.Button1.Text = "Зашифровать";
            this.Button1.UseVisualStyleBackColor = false;
            this.Button1.Click += new System.EventHandler(this.MaterialRaisedButton1_Click);
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(301, 77);
            this.materialLabel1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(188, 27);
            this.materialLabel1.TabIndex = 8;
            this.materialLabel1.Text = "Исходный  текст";
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(263, 437);
            this.materialLabel2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(266, 27);
            this.materialLabel2.TabIndex = 9;
            this.materialLabel2.Text = "Расшифрованный текст";
            // 
            // Button3
            // 
            this.Button3.Depth = 0;
            this.Button3.Location = new System.Drawing.Point(784, 122);
            this.Button3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Button3.MouseState = MaterialSkin.MouseState.HOVER;
            this.Button3.Name = "Button3";
            this.Button3.Primary = true;
            this.Button3.Size = new System.Drawing.Size(141, 110);
            this.Button3.TabIndex = 10;
            this.Button3.Text = "Загрузка из файла";
            this.Button3.UseVisualStyleBackColor = true;
            this.Button3.Click += new System.EventHandler(this.MaterialRaisedButton3_Click);
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(276, 249);
            this.materialLabel3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(253, 27);
            this.materialLabel3.TabIndex = 11;
            this.materialLabel3.Text = "Зашифрованный текст";
            // 
            // OpenTextField1
            // 
            this.OpenTextField1.Depth = 0;
            this.OpenTextField1.Hint = "";
            this.OpenTextField1.Location = new System.Drawing.Point(36, 659);
            this.OpenTextField1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.OpenTextField1.MouseState = MaterialSkin.MouseState.HOVER;
            this.OpenTextField1.Name = "OpenTextField1";
            this.OpenTextField1.PasswordChar = '\0';
            this.OpenTextField1.SelectedText = "";
            this.OpenTextField1.SelectionLength = 0;
            this.OpenTextField1.SelectionStart = 0;
            this.OpenTextField1.Size = new System.Drawing.Size(254, 32);
            this.OpenTextField1.TabIndex = 12;
            this.OpenTextField1.UseSystemPasswordChar = false;
            // 
            // TextBox3
            // 
            this.TextBox3.Location = new System.Drawing.Point(18, 293);
            this.TextBox3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.TextBox3.Name = "TextBox3";
            this.TextBox3.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.TextBox3.Size = new System.Drawing.Size(758, 125);
            this.TextBox3.TabIndex = 13;
            this.TextBox3.Text = "";
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(67, 627);
            this.materialLabel4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(180, 27);
            this.materialLabel4.TabIndex = 14;
            this.materialLabel4.Text = "Открытый ключ";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(460, 627);
            this.materialLabel5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(162, 27);
            this.materialLabel5.TabIndex = 15;
            this.materialLabel5.Text = "Закртый ключ";
            // 
            // CloseTextField2
            // 
            this.CloseTextField2.Depth = 0;
            this.CloseTextField2.Hint = "";
            this.CloseTextField2.Location = new System.Drawing.Point(422, 659);
            this.CloseTextField2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.CloseTextField2.MouseState = MaterialSkin.MouseState.HOVER;
            this.CloseTextField2.Name = "CloseTextField2";
            this.CloseTextField2.PasswordChar = '\0';
            this.CloseTextField2.SelectedText = "";
            this.CloseTextField2.SelectionLength = 0;
            this.CloseTextField2.SelectionStart = 0;
            this.CloseTextField2.Size = new System.Drawing.Size(254, 32);
            this.CloseTextField2.TabIndex = 16;
            this.CloseTextField2.UseSystemPasswordChar = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(966, 758);
            this.Controls.Add(this.CloseTextField2);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.TextBox3);
            this.Controls.Add(this.OpenTextField1);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.Button3);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.materialLabel1);
            this.Controls.Add(this.Button1);
            this.Controls.Add(this.Button2);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.TextBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "RSA";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox TextBox1;
		private System.Windows.Forms.RichTextBox richTextBox2;
		private MaterialSkin.Controls.MaterialRaisedButton Button2;
		private MaterialSkin.Controls.MaterialRaisedButton Button1;
		private MaterialSkin.Controls.MaterialLabel materialLabel1;
		private MaterialSkin.Controls.MaterialLabel materialLabel2;
		private MaterialSkin.Controls.MaterialRaisedButton Button3;
		private MaterialSkin.Controls.MaterialLabel materialLabel3;
		private MaterialSkin.Controls.MaterialSingleLineTextField OpenTextField1;
		private System.Windows.Forms.RichTextBox TextBox3;
		private MaterialSkin.Controls.MaterialLabel materialLabel4;
		private MaterialSkin.Controls.MaterialLabel materialLabel5;
		private MaterialSkin.Controls.MaterialSingleLineTextField CloseTextField2;
	}
}

